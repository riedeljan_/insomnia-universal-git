# Universal Git Plugin for Insomnia API Client

This plugin for Insomnia aims to ease syncing your workspaces, directories or even single requests to your Git repositories. Right now GitLab is supported only but GitHub and others will be supported soon.

## Current Features
### GitLab
*   Sync Workspace

## How to use this plugin

Just install it via the [Insomnia Plugin Hub](https://insomnia.rest/plugins).

Alternatively you can clone this repository and to your local Insomnia plugin directory. Its location might differ based on your OS. You can find it out via `Insomnia -> Preferences -> Plugins -> Reveal Plugins Folder`.

After installing just hit the dropdown menu located right beneath the workspace/collections name, run through the setup and start pulling/pushing your config.
